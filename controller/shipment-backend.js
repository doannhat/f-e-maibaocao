const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017/shipment-database";
const fs = require('fs');

exports.getQuote = (req, res) => {
    // res.header("Access-Control-Allow-Origin", "*");

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    // res.setHeader('Access-Control-Allow-Credentials', true);
    MongoClient.connect(url, (err, db) => {
        let shipmentBody = req.data;
        if (err) throw new Error(err);
        console.log("Database connected.");
        console.log(shipmentBody);
        //connect to database
        let dbo = db.db('shipment-database');

        //check country's validation
        let destinationCountry = shipmentBody.destination.address.country_code;
        let originCountry = shipmentBody.origin.address.country_code;
        if (destinationCountry !== "FR" || originCountry !== "FR") {
            console.log("Our service supports shipments in France only.")
        }

        //get rate info
        let package = shipmentBody.package;
        w = 0;
        if (package.grossWeight.unit === "kg") {
            console.log("KG");
            w = package.grossWeight.amount * 1000;
        } else {
            console.log("G");
            w = package.grossWeight.amount;
        }
        if (w > 15000) {
            w = 15500;
        }
        console.log(w);
        rate = dbo.collection('rates').findOne({
            "weight_maxi": { $gte: w }
        }, ['_id', 'price'], (err, data) => {
            if (err) throw new Error(err);
            console.log(555);
            resp = [
                {
                    "id": data._id,
                    "amount": data.price //EUR
                }
            ];
            // res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
            // res.header("Access-Control-Allow-Headers", "*");
            // res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');

          
            res.json(_ResponseData(resp));

        });
        db.close();
    })

};

exports.createShipment = (req, res) => {
    MongoClient.connect(url, (err, db) => {
        try {
            let shipmentBody = req.data;
            if (err) throw new Error(err);
            let dbo = db.db('shipment-database');

            let weight = shipmentBody.package.grossWeight;
            let w = 0;
            if (weight.unit === "kg") {
                w = weight.amount * 1000;
            } else {
                w = weight.amount;
            }
            if (w > 15000) {
                w = 15500;
            }
            rate = dbo.collection('rates').findOne({
                "weight_maxi": { $gte: w }
            }, ['_id', 'price'], (err, data) => {
                if (err) {
                    res.json(_ResponseData({}));
                };
                resp = {
                    "ref": _RandomRefNumber(10),
                    "created_at": new Date(),
                    "cost": data.price //EUR
                };
                res.json(_ResponseData(resp));
                resp = {
                    "ref": _RandomRefNumber(10),
                    "created_at": new Date(),
                    "cost": data.price, //EUR
                    "shipment info": shipmentBody
                };
                dbo.collection('shipments').insertOne(resp, (err, res) => {
                    if (err) throw new Error(err);
                })
            });

        } catch (e) {
            db.close();
        }
    })
}

exports.getShipment = (req, res) => {
    MongoClient.connect(url, (err, db) => {
        ref = req.body.data.ref;
        console.log(ref);
        if (err) throw new Error(err);
        let dbo = db.db('shipment-database');
        shipment = dbo.collection('shipments').findOne({
            "ref": ref
        }, ['ref', 'shipment_info'], (err, data) => {
            if (err) throw new Error(err);
            if (data === null) {
                resp = {
                    "ref": ""
                }
                res.json(_ResponseData(resp));
            } else {
                origin = data.shipment_info.origin;
                destination = data.shipment_info.destination;
                package_info = data.shipment_info.package;
                resp = {
                    "ref": ref,
                    "origin": {
                        "contact": {
                            "name": origin.contact.name,
                            "email": origin.contact.email,
                            "phone": origin.contact.phone,
                        },
                        "address": {
                            "country_code": origin.address.country_code,
                            "locality": origin.address.locality,
                            "postal_code": origin.address.postal_code,
                            "address_line1": origin.address.address_line1,
                        }
                    },
                    "destination": {
                        "contact": {
                            "name": destination.contact.name,
                            "email": destination.contact.email,
                            "phone": destination.contact.phone,
                        },
                        "address": {
                            "country_code": destination.address.country_code,
                            "locality": destination.address.locality,
                            "postal_code": destination.address.postal_code,
                            "address_line1": destination.address.address_line1,
                        }
                    },
                    "package": {
                        "dimensions": {
                            "height": package_info.dimensions.height,
                            "width": package_info.dimensions.width,
                            "length": package_info.length,
                            "unit": package_info.dimensions.unit,
                        },
                        "grossWeight": {
                            "amount": package_info.grossWeight.amount,
                            "unit": package_info.grossWeight.unit,
                        }

                    }
                }
                res.json(_ResponseData(resp));
            }
        });
        db.close();
    })
}

exports.deleteShipment = (req, res) => {
    MongoClient.connect(url, (err, db) => {
        if (err) throw new Error(err);
        let dbo = db.db('shipment-database');
        ref = req.body.data.ref;
        dbo.collection('shipments').deleteOne({"ref":ref}, (err, result) => {
            if (err) throw new Error(err);
            // console.log(result);
            resp = {
                "status": err? "NOK" : "OK",
                "message": err? "Shipment not found" : `Shipment ${ref} has been deleted`
            }
            res.json(_ResponseData(resp));
        })
        db.close();
    })
}

function _RandomRefNumber(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function _ResponseData(data = {}) {
    return {
        "data": data ? data : {},
    };
};